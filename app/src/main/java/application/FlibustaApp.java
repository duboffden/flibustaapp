package application;

import android.app.Application;

import components.AppComponent;
import components.DaggerAppComponent;
import modules.AppModule;
import modules.NetModule;
import modules.ProvidersModule;
import com.facebook.stetho.Stetho;

/**
 * Created by Dubovitsky Denis on 1/17/2017.
 */

public class FlibustaApp extends Application {
    private AppComponent sAppComponent;
    @Override
    public void onCreate() {
        super.onCreate();

        Stetho.initializeWithDefaults(this);

        sAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .providersModule(new ProvidersModule())
                .netModule(new NetModule())
                .build();
    }

    public AppComponent getAppComponent() {
        return sAppComponent;
    }
}
