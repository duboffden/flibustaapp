package interfaces;

import models.Book;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Denis Dubovitsky on 1/16/2017.
 *
 */

public interface BooksApiService {
    @GET("php/GetBooks.php")
    Observable<List<Book>> getBooks(@Query("t") String bookName);

}
