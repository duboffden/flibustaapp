package adapters;

import android.content.Context;
import android.content.Intent;
import android.provider.SyncStateContract;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;

import com.example.shpp_admin.flibustaapp.R;

import constants.Constants;
import utils.BooksDownloader;
import application.FlibustaApp;
import holders.BookHolder;
import models.Book;
import providers.BooksProvider;

import javax.inject.Inject;

/**
 * Created by Dubovitsky Denis on 1/17/2017.
 * Adapter for RecyclerView to show BooksList in MainActivity
 */

public class BooksListAdapter extends RecyclerView.Adapter<BookHolder> {
    @Inject
    BooksProvider sProvider;

    private BooksDownloader sDownloader;
    private Context sContext;

    public BooksListAdapter(Context context) {
        super();

        ((FlibustaApp) context)
                .getAppComponent()
                .inject(this);
        sContext = context;
    }

    @Override
    public BookHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new BookHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.holder_book, parent, false));
    }

    @Override
    public void onBindViewHolder(BookHolder holder, int position) {
        Book book = sProvider.getList().get(position);
        holder.getAuthorName().setText(book.getFullName());
        holder.getBookName().setText(book.getTitle());

        if (book.getIsDownloaded())
            holder.getDownload().setImageResource(R.drawable.icon_downloaded);
        else
            holder.getDownload().setImageResource(R.drawable.icon_download);

        holder.getDownload().setOnClickListener(v -> {
            sProvider.downloadBook(book.getBookId(), position, book.getTitle());
        });

        holder.getRoot().setOnClickListener(v -> {
            if (book.getIsDownloaded()) {
                if (book.getBookUri() == null)
                    return;

                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(book.getBookUri(), MimeTypeMap.getSingleton().getMimeTypeFromExtension("fb2"));
                intent = Intent.createChooser(intent, sContext.getString(R.string.open_in));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                sContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return sProvider.getList().size();
    }
}


//***************************Legacy o.o***************************************
//Intent intent = new Intent(Intent.ACTION_VIEW);
//intent.setDataAndType(uri, mimeType);
//        intent = Intent.createChooser(intent, context.getString(R.string.open_in));
//        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//
//        context.startActivity(intent);