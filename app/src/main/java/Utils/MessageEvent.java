package utils;

import constants.Messages;


public class MessageEvent {
    private Messages message;
    private Object delivery;

    public MessageEvent(Messages message) {
        this.message = message;
    }

    public MessageEvent(Messages message, Object delivery){
        this.message = message;
        this.delivery = delivery;
    }

    public Messages getMessage() {
        return message;
    }

    public Object getDelivery() {
        return delivery;
    }
}