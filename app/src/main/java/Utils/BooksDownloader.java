package utils;

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.webkit.MimeTypeMap;

import constants.Constants;
import receivers.DownloadReceiver;

/**
 * A BooksDownloader.class is used to download from uri;
 * Used in ControlFragment to download books;
 */

public class BooksDownloader {
    private DownloadManager mDownloadManager;
    private DownloadReceiver mDownloadReceiver = null;
    private Context sContext;

    public BooksDownloader(Context sContext) {
        this.sContext = sContext;
        mDownloadManager = (DownloadManager) sContext.getSystemService(Context.DOWNLOAD_SERVICE);
    }

    public long download(int bookId, String bookName) {
        if (mDownloadReceiver == null) {
            mDownloadReceiver = new DownloadReceiver();
            mDownloadReceiver.register(sContext);
        }

        return downloadBook(getUrlForBookDownloading(bookId), bookName);
    }

    private long downloadBook(String downloadingUrl, String bookName) {
        if (downloadingUrl == null)
            return Constants.WRONG_VALUE;

        if (bookName == null)
            return Constants.WRONG_VALUE;

        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(downloadingUrl));
        request.setMimeType(MimeTypeMap.getSingleton().getMimeTypeFromExtension(Constants.FB2_FILE_EXTENSION));
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, formatBookName(bookName) + Constants.FB2_FILE_EXTENSION);
        return mDownloadManager.enqueue(request);
    }

//    @Override
//    public void downloadComplete(long completedDownloadId) {
//            DownloadManager.Query query = new DownloadManager.Query();
//            query.setFilterById(completedDownloadId);
//            Cursor cursor = mDownloadManager.query(query);
//
//            while (cursor.moveToNext())
//                getFileInfo(cursor);
//
//            cursor.close();
//    }

    public void unregister() {
        if (mDownloadReceiver != null)
            mDownloadReceiver.unregister(sContext);

        mDownloadReceiver = null;
    }

//    private void getFileInfo(Cursor cursor) {
//        int status = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS));
//        if (status == DownloadManager.STATUS_SUCCESSFUL) {
//            Long id = cursor.getLong(cursor.getColumnIndex(DownloadManager.COLUMN_ID));
//            if (id != downloadId)
//                return;
//
//            Uri uri = mDownloadManager.getUriForDownloadedFile(downloadId);
//            listener.fileDownloaded(uri, MimeTypeMap
//                    .getSingleton()
//                    .getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI)))));
//        }
//    }

    private String getUrlForBookDownloading(int bookId) {
        return Constants.DOWNLOAD_BOOK_API_URL + bookId + Constants.DOWNLOAD_BOOK_API_BOOK_FORMAT;
    }

    private String formatBookName(String bookName) {
        if (bookName == null)
            return Constants.EMPTY_STRING;

        if (bookName.isEmpty())
            return Constants.EMPTY_STRING;

        return bookName.replace(".", Constants.EMPTY_STRING);
    }

    public Uri getDownloadedUriFromDownloadId(long downloadId){
        return mDownloadManager.getUriForDownloadedFile(downloadId);
    }
}