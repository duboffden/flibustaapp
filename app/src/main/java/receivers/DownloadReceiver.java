package receivers;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;

import constants.Messages;
import utils.MessageEvent;

/**
 * A BooksDownloader.class is used to download from uri;
 * Used in ControlFragment to download books;
 */

public class DownloadReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("---", "Doownloaded ");
        EventBus.getDefault().post(new MessageEvent(Messages.DOWNLOADED, intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1)));
    }

    public void register(Context context) {
        IntentFilter downloadFilter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        context.registerReceiver(this, downloadFilter);
    }

    public void unregister(Context context) {
        context.unregisterReceiver(this);
    }
}