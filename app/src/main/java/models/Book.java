package models;

import android.net.Uri;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Denis Dubovitsky on 1/16/2017.
 *
 */

public class Book {
    @SerializedName("BookId")
    private int bookId;
    @SerializedName("Title")
    private String title;
    @SerializedName("FirstName")
    private String authors_first_name;
    @SerializedName("LastName")
    private String authors_last_name;

    private long downloadingId;
    private boolean isDownloaded;

    private Uri bookUri;
    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthors_first_name() {
        return authors_first_name;
    }

    public void setAuthors_first_name(String authors_first_name) {
        this.authors_first_name = authors_first_name;
    }

    public String getAuthors_last_name() {
        return authors_last_name;
    }

    public void setAuthors_last_name(String authors_last_name) {
        this.authors_last_name = authors_last_name;
    }

    public String getFullName() {
        if (authors_first_name != null && authors_last_name != null) {
            return authors_first_name + " " + authors_last_name;
        } else
            return "Неизвестный Автор";
    }

    public long getDownloadingId() {
        return downloadingId;
    }

    public void setDownloadingId(long downloadingId) {
        this.downloadingId = downloadingId;
    }

    public boolean getIsDownloaded() {
        return isDownloaded;
    }

    public void setIsDownloaded(boolean downloaded) {
        isDownloaded = downloaded;
    }

    public Uri getBookUri() {
        return bookUri;
    }

    public void setBookUri(Uri bookUri) {
        this.bookUri = bookUri;
    }
}
