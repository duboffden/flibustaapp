package activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;

import com.example.shpp_admin.flibustaapp.R;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;

import utils.BooksDownloader;
import utils.MessageEvent;
import adapters.BooksListAdapter;
import application.FlibustaApp;
import butterknife.BindView;
import butterknife.ButterKnife;
import providers.BooksProvider;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String TAG = "MainActivity";

    @BindView(R.id.et_book_name)
    EditText etBookName;
    @BindView(R.id.rv_books_list)
    RecyclerView rvBooksList;

    @Inject
    BooksProvider sBooksProvider;
    @Inject
    BooksDownloader sBooksDownloader;

    BooksListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        ((FlibustaApp) getApplication())
                .getAppComponent()
                .inject(this);

        setupBooksList();

        findViewById(R.id.btn_search).setOnClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onClick(View v) {
        String bookRequest = etBookName.getText().toString();
        if (!bookRequest.isEmpty())
            sBooksProvider.provideBooks(bookRequest);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        switch (event.getMessage()) {
            case BOOKS_UPDATED:
                mAdapter.notifyDataSetChanged();
                break;
            case DOWNLOADED:
                sBooksProvider.bookDownloaded((Long) event.getDelivery());
        }
    }

    @Override
    protected void onDestroy() {
        sBooksDownloader.unregister();
        super.onDestroy();
    }

    private void setupBooksList() {
        mAdapter = new BooksListAdapter(getApplicationContext());
        rvBooksList.setLayoutManager(new LinearLayoutManager(this));
        rvBooksList.setAdapter(mAdapter);
        rvBooksList.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
    }


}
