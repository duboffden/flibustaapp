package constants;

/**
 * Created by Dubovitsky Denis on 1/17/2017.
 *
 */

public class Constants {
    public static final String BASE_API_URL = "https://flibs.site/";
    public static final String DOWNLOAD_BOOK_API_URL = "http://flibs.site/d?b=";
    public static final String FB2_FILE_EXTENSION = ".fb2";
    public static final String DOWNLOAD_BOOK_API_BOOK_FORMAT = "&f=fb2";
    public static final String EMPTY_STRING = "";
    public static final int WRONG_VALUE = -1;
}
