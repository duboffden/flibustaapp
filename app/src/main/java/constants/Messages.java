package constants;


public enum  Messages {
    BOOKS_UPDATED,
    NO_BOOKS_FOUND,
    ERROR_DURING_BOOKS_DOWNLOADING,
    DOWNLOADED
}
