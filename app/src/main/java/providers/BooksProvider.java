package providers;

import android.content.Context;
import android.util.LongSparseArray;

import utils.BooksDownloader;
import utils.MessageEvent;
import application.FlibustaApp;
import constants.Messages;
import interfaces.BooksApiService;
import models.Book;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Retrofit;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static constants.Constants.WRONG_VALUE;

/**
 * Created by Dubovitsky Denis on 1/17/2017.
 * A provider is used to provide books to BooksListAdapter
 */

public class BooksProvider {
    @Inject
    Retrofit sRetrofit;
    @Inject
    BooksDownloader sBooksDownloader;

    private ArrayList<Book> mBooksList = new ArrayList<>();
    private LongSparseArray<Integer> mBooksDownloadIdPositionMap = new LongSparseArray<>();// array that maps download Id given by download manager to position in books list

    public BooksProvider(Context context) {
        ((FlibustaApp) context
                .getApplicationContext()
                .getApplicationContext())
                .getAppComponent()
                .inject(this);
    }

    public void provideBooks(String bookName) {
        if (bookName == null)
            return;

        if (bookName.isEmpty())
            return;

        sRetrofit.create(BooksApiService.class)
                .getBooks(bookName)
                .subscribeOn(Schedulers.newThread()) // request books in new thread
                .observeOn(AndroidSchedulers.mainThread()) // return result to main thread
                .subscribe(this::setupNewBooks, this::handleExceptionDuringBookListLoading); // first function is called if our request is success, second if not
    }

    private void setupNewBooks(List<Book> newBooks) {
        mBooksList.clear();
        mBooksDownloadIdPositionMap = new LongSparseArray<>();

        if (newBooks == null || newBooks.isEmpty()) {
            EventBus.getDefault()
                    .post(new MessageEvent(Messages.NO_BOOKS_FOUND));
            return;
        }

        mBooksList.addAll(newBooks);

        EventBus.getDefault()
                .post(new MessageEvent(Messages.BOOKS_UPDATED));
    }

    private void handleExceptionDuringBookListLoading(Throwable t) {
        t.printStackTrace();

        EventBus.getDefault()
                .post(new MessageEvent(Messages.ERROR_DURING_BOOKS_DOWNLOADING));
    }

    public ArrayList<Book> getList() {
        return mBooksList;
    }

    public void downloadBook(int bookId, int bookPosition, String bookName) {
        if (bookName == null)
            return;

        if (bookName.isEmpty())
            return;

        long downloadId = sBooksDownloader.download(bookId, bookName);

        if (downloadId != WRONG_VALUE)
            mBooksDownloadIdPositionMap.put(downloadId, bookPosition);
    }

    public void bookDownloaded(long downloadId) {
        if (downloadId < 0)
            return;

        int bookPosition = mBooksDownloadIdPositionMap.get(downloadId, WRONG_VALUE);

        if (bookPosition == WRONG_VALUE)
            return;
        Book book = mBooksList.get(bookPosition);
        book.setIsDownloaded(true);
        book.setBookUri(sBooksDownloader.getDownloadedUriFromDownloadId(downloadId));
        EventBus.getDefault()
                .post(new MessageEvent(Messages.BOOKS_UPDATED));
    }
}
