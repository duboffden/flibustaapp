package modules;

import android.app.Application;

import utils.BooksDownloader;
import providers.BooksProvider;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;


@Module
public class ProvidersModule {

    @Provides
    @Singleton
    BooksProvider provideBooksProvider(Application application){
        return new BooksProvider(application);
    }

    @Provides
    @Singleton
    BooksDownloader provideBooksDownloader(Application application){
        return new BooksDownloader(application);
    }
}
