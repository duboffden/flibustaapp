package components;

import activity.MainActivity;
import adapters.BooksListAdapter;
import modules.AppModule;
import modules.NetModule;
import modules.ProvidersModule;
import providers.BooksProvider;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Dubovistky Denis on 1/17/2017.
 */

@Singleton
@Component(modules={NetModule.class, AppModule.class, ProvidersModule.class})
public interface AppComponent {
    void inject(MainActivity activity);
    void inject(BooksProvider provider);
    void inject(BooksListAdapter adapter);
}