package holders;

import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.shpp_admin.flibustaapp.R;


public class BookHolder extends RecyclerView.ViewHolder {
    private TextView bookName;
    private TextView authorName;
    private AppCompatImageView download;
    private ConstraintLayout root;
    public BookHolder(View itemView) {
        super(itemView);
        bookName = (TextView) itemView.findViewById(R.id.tv_book_name);
        authorName = (TextView) itemView.findViewById(R.id.tv_author_name);
        download = (AppCompatImageView) itemView.findViewById(R.id.btn_download);
        root = (ConstraintLayout) itemView.findViewById(R.id.cl_book_root);
    }

    public TextView getBookName() {
        return bookName;
    }

    public TextView getAuthorName() {
        return authorName;
    }

    public AppCompatImageView getDownload() {
        return download;
    }

    public ConstraintLayout getRoot() {
        return root;
    }
}
